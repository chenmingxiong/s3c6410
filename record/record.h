#ifndef __RECORD_H
#define __RECORD_H

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/soundcard.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/vfs.h>
//#include <setjmp.h>
#include <string.h>

enum CMD_NAME{CMD_START=0, CMD_PAUSE, CMD_QUIT, CMD_TIME, CMD_NULL};
typedef struct tagCOMMANDINFO		// command informa
{
	int iID;		// command id	
	int iRecv;
	int iSend;
	char *pRecv;
	char *pSend;
}CommandInfo;

#define SIZE_CMD		24		// command len
#define FD_RECV			0		// recv
#define FD_SEND			1		// send
typedef struct tagCOMMANDCTRL	// command ctrl (send or recv)
{
	int fd[2];		// 0 is recv, 1 is: send
	int iLen;		// command len;
	unsigned char sCmd[SIZE_CMD];		// send command
}CmdCtrl;


// recrod ctrl init cmd ctrl
int rcInitCmdCtrl(CmdCtrl *pCC);

// record ctrl close
void rcCloseCmdCtrl(CmdCtrl *pCC);

// record send command
// in : iVal: -1 no param
// ret: 0 Error, else ok
int rcSendCommand(CmdCtrl *pCC, int iCmd, int iVal);

// record ctrl Cmd start
// ret: 0 Error, else ok
//int rcCmdQuit(void);

// get ctrl command
int rcGetCommand(void);

#endif	// __RECORD_H
