#ifndef _PRINT_DEF_H_
#define _PRINT_DEF_H_

#define PRINT_ERROR	1	// error message
#define PRINT_FUNC	1	// func entry
#define PRINT_MESSG 1	// print message for user success
//#define PRINT_ASSERT 1	// test some is not null

#define PVERSION(fmt, args...) \
	{	printf("\033[01;37;30m"); \
		printf(fmt, ##args); \
		printf(" Build time: %s, %s.\n\033[0m", __DATE__, __TIME__); \
		fflush(stdout);}

#ifdef PRINT_ERROR
#define PERROR(fmt, args...) \
	{	printf("**ERROR In:%s():%4d> ", __FUNCTION__, __LINE__); \
		printf(fmt, ##args);	fflush(stdout);}
#else
#define PERROR(fmt, args...)
#endif				// PRINT_ERROR

#ifdef PRINT_FUNC
#define	PFUNC(fmt, args...) \
	{	printf("< Enter In:%s():%4d> ", __FUNCTION__, __LINE__); \
		printf(fmt, ##args);	fflush(stdout);}
#else
#define PFUNC(fmt, args...)
#endif				// PRINT_FUNC

#ifdef PRINT_MESSG
#define PMSG(fmt, args...) \
	{	printf("<%s>:%d ", __FILE__, __LINE__); \
		printf(fmt, ##args); 	fflush(stdout);}
#else
#define PMSG(fmt, args...)
#endif				// PRINT_MESSG

#ifdef PRINT_ASSERT
#define	ASSERT(a)	{if(!(a)){ printf("<%s>:%d, assert(false)\n", __FILE__, __LINE__); \
	fflush(stdout);		while(1)	;}}
#else
#define ASSERT(a)
#endif				// PRINT_ASSERT


#endif				//_PRINT_DEF_H_
