录音文件的几个重要参数设置：

00  BYTES  sIDriff RIFF 标志
04  WORD   dwSizeriff = %d // 文件长度-8
08  BYTES  sFormat    = %s // 文件类型 必须是WAVE
12  BYTES  sIDfmt     = %s" // "fmt "
16  DWROD  dwSizefmt  = %d" ,riffHead.dwSizefmt);
20  WORD   wFormatTag      = %d(0x%X)"
22  WORD   nChannels       = %d(0x%X)  立体声
24  DWORD  nSamplesPerSec  = %d(0x%X)  采样率
28  DWORD  nAvgBytesPerSec = %d(0x%X)  数据量/s
32  WORD   nBlockAlign     = %d(0x%X)",
34  WORD   wBitsPerSample  = %d(0x%X)",
36  WORD   cbSize          = %d(0x%X)", wavfmt.cbSize, wavfmt.cbSize);
38  WORD   dExdata         = %d", dExedata
40  BYTES  sID             = %s", "fact"
44  DWORD  dwTag           = %d(0x%X)", fact.dwTag, fact.dwTag);
48  DWORD  dwSize          = %d(0x%X)", fact.dwSize, fact.dwSize);
52  BYTES  sID             = %s", "data"
56  DWORD  dwSize          = %d(0x%X)  数据长度"
60 


序  采样   CH  数据量/s   对齐    wBitsPerSample  nSamplePerBlock   size(K/20sec)
01  8000    1   4055      256          4           505   		 	  81
02          2   8100      512          4           505     			 162

03  11025   1   5588      256          4           505     			 111
04          2   11177     512          4           505     			 223

05  22050   1   11100     512          4           1017    			 221
06          2   22201     1024         4           1017    			 442

07  44100   1   22125     1024         4           2041    			 440
08          2   44251     2048         4           2041    			 880


nAvgBytesPerSec = %d(0x%X)  数据量/s


在BF533上做了一部分的修改，在调试的过程中，主要问题出现在
	对SPORT来模拟I2S的寄存器设置有问题。8750可能也有问题，调试了很长时间，也没有结果。
	在2.6的内核上我们的录音程序也作了一部分的修改，主要是对DSP增加了几个ioctl的调用。
