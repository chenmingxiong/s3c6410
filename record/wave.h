// wave.h
// Play wave file 
// it can play WAVE_FORMAT_IMA_ADPCM=17 ro WAVE_FORMAT_PCM=1
// by WangGang

#ifndef __WAVE_PLAY_H
#define __WAVE_PLAY_H

#define SARCO		1
#define USE_SWAP	1			// in some little or big CPU

#ifdef USE_SWAP
//***********************************************************************************************
//this is use for swap for pc or 5249
//***********************************************************************************************
#define SWAPLONG(a) ((((a) & 0xff000000) >> 24) | \
					 (((a) & 0x00ff0000) >> 8) | \
					 (((a) & 0x0000ff00) << 8) | \
					 (((a) & 0x000000ff) << 24))
#define SWAPSHORT(a)    (((a) >> 8) | ((a) << 8))
#define SWAPCHAR(a) ((((a) & 0xf0) >> 4) | \
				     (((a) & 0x0f) << 4))

#define CLAMP_S16(x)  if (x < -32768) x = -32768; \
else if (x > 32767) x = 32767;

#define CLAMP_0_TO_88(x)  {if (x < 0) x = 0; else if (x > 88) x = 88;}
#endif // USE_SWAP

//--------------------------------------------------------------------------------------
//   next is only for wave.c
//--------------------------------------------------------------------------------------
// current it play format,support define here
#define WAVE_FORMAT_IMA_ADPCM		0x11
#define WAVE_FORMAT_PCM				0x01


// Define the WAV file magic numbers.
#define	WAV_RIFFTYPE	0x52494646		// "RIFF"
#define	WAV_WAVETYPE	0x57415645		// "WAVE"
#define	WAV_FMTTYPE		0x666d7420		// "fmt "
#define	WAV_DATATYPE	0x64617461		// "data"
#define	WAV_FACTTYPE	0x66616374		// "fact"

#if 0
// Intel ADPCM step variation table 
static int indexTable[16] = {
	-1, -1, -1, -1, 2, 4, 6, 8,
	-1, -1, -1, -1, 2, 4, 6, 8,
};

// table for decode or encode
static int stepsizeTable[89] = {
	7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
	19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
	50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
	130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
	337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
	876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
	2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
	5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
	15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
};
#endif

// first 16 BYTE of the file
typedef struct tagRIFFHDR {
	unsigned long riffid;			// 00(00H)		must be "RIFF"
	unsigned long len;				// 04(04H)		filelen - 8
	unsigned long waveid;			// 08(08H)		must be "WAVE"
	unsigned long fmtid;			// 12(0CH)		must be "fmt "
	unsigned long hdrlen;			// 16(10H)		size of nexe == IMA_PCM(16 or 20) PCM(18)
}RIFFHDR;

// file format define
typedef struct tagWAVEFORMATEX {
	unsigned short  wFormatTag; 	// 20(14H)		must be 1 or 17
	unsigned short  nChannels; 		// 22(16H)		must be 1 or 2
	unsigned long	nSamplesPerSec; // 24(18H)		current bitrate
	unsigned long	nAvgBytesPerSec;// 28(1CH)		avg byte per sec
	unsigned short	nBlockAlign; 	// 32(20H)		block
	unsigned short	wBitsPerSample; // 34(22H)		
	unsigned short	cbSize; 		// 36(24H)		if hdrlen == 20, it=2 else none
	unsigned short	nSamplesPerBlock;// 38(26H)		
} WAVEFORMATEX; 

// Define the basic wave file header structure.
typedef struct tagWAVEFILEHDR {
	RIFFHDR 		riffhdr;
	WAVEFORMATEX 	format;	
}WAVEFILEHDR;

// this thrunk may be not
typedef struct tagWAVEFACTHDR {
	unsigned long factid;			// "fact"
	unsigned long size;				//  
	unsigned long SampleLength;		// 
}WAVEFACTHDR;

// this is last chunk
typedef struct tavWAVEDATAHDR {
	unsigned long dataid;			// "data" 					//	0x24
	unsigned long size;				// Data size 				//	0x28
}WAVEDATAHDR;


typedef struct tagADPCM_STATE {
	short valprev;		/*Previous output value */
	char index;			/*Index into stepsize table */
}ADPCM_STATE;

typedef struct tagIMA_BLOCKHDR {
	short iSample0;
	unsigned char  bStepTableIndex;
	unsigned char  bReserved;
}IMA_BLOCKHDR;


//**********************************************************************************
//
//**********************************************************************************

void InitBR8kTable();
void FreeBR8kTable();

// convert samplerate from 44100 to ...(iSampleRate)		must be 16 bits
// in:	iChannels:	channel num(1 or 2)
//		iDataLen: iDataLen in pInData
//		iSampleRate: to 
//		pInData, pOutData
// ret: -1(Error) other(pOutData len)
//
int SampleRateConvert(int iChannels, int iDataLen, int iSampleRate, void *pInData, void *pOutData);


#endif 		//__WAVE_PLAY_H
