#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "wave.h"

unsigned short *p_BR_8K_table = NULL;


void InitBR8kTable()
{
	int i = 0;
	double dVal = 44100 / 8000;
	
	if( p_BR_8K_table )
		return;

	p_BR_8K_table = (unsigned short*)malloc(1024 * 8);

	for(i = 0; i < 1024 * 4; i++)
	{
		p_BR_8K_table[i] = (unsigned short)(dVal * ((double)i) );
	}
}

void FreeBR8kTable()
{
	if( p_BR_8K_table)
		free(p_BR_8K_table), p_BR_8K_table = NULL;
}

// convert samplerate from 44100 to ...(iSampleRate)		must be 16 bits
// in:	iChannels:	channel num(1 or 2)
//		iDatalen: Data num in pInData
//		iSampleRate: to 
//		pInData, pOutData
// ret: -1(Error) other(pOutData len)
//
int SampleRateConvert(int iChannels, int iDataLen, int iSampleRate, void *pInData, void *pOutData)
{
	int iRet = -1, i;
	int iInterVal;

	switch(iSampleRate)
	{
	case 44100:
		return iDataLen;
	case 22050:
		iInterVal = 2;
		break;
	case 11025:
		iInterVal = 4;
		break;
	case 8000:
		goto BR_8000;
	default:		//	don't run here
		return iDataLen;
	}
	
	iRet = iDataLen / 4 / iInterVal;

	for(i = 0; i < iRet; i++)
	{
		((unsigned long *)pOutData)[i] = ((unsigned long *)pInData)[i*iInterVal];
	}
	return (iRet * 4);
	
BR_8000:

	{
		double dInterVal = 44100 / 8000;
		iRet = (int)((iDataLen / 4) / dInterVal);
		if( iRet > 1024 * 4)
		{
			printf(" Error iRet = %d\n", iRet);
		}
		
		for(i = 0; i < iRet; i++)
		{
			((unsigned long *)pOutData)[i] = ((unsigned long*)pInData)[p_BR_8K_table[i]];
		}	
	}
	return (iRet * 4);
}

