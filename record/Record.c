#include "record.h"
#include "ima_rw.h"
#include "wave.h"
#include "printdef.h"

#ifdef PRINT_ERROR
  #include <errno.h>
#endif

#define BLOCK_SIZE		1024			// 1k

#ifdef USE_PTHREAD
#include <pthread.h>
#define BUFFER_CNT	2
typedef enum {DO_NONE=0, DO_QUIT, DO_WRITE1, DO_WRITE2}do_info;

typedef struct tagTHREADPARAM	
{
	short			use, iDoing;				// the thread current do
	pthread_t		pth;
	pthread_attr_t  pth_attr; 
	FILE			*fp;				// write file point
	int 			usleep;				// usleep time
	unsigned long   WrAddrs[BUFFER_CNT];
	unsigned long   WrLens[BUFFER_CNT];
	unsigned long   WrNo, WrCnt;		// Read buffer, allread buffercnt
}ThreadParam;


ThreadParam gThreadParam = {0,DO_NONE};
int pth_write_file(void *param)
{
	int iNo = 0;
	ThreadParam *pTP = (ThreadParam*)param;

	PFUNC("Start pth_write_file\n");

	while(1)	{
		switch( pTP->iDoing)
		{
		case DO_NONE:
			usleep(pTP->usleep);
			break;
		case DO_WRITE1:
		case DO_WRITE2:
			if(pTP->WrNo <= pTP->WrCnt)	{
				usleep(pTP->usleep);
				continue;
			}
			iNo = pTP->iDoing - DO_WRITE1;
			fwrite((void*)pTP->WrAddrs[iNo], pTP->WrLens[iNo], 1, pTP->fp);
			fsync(fileno(pTP->fp));
			pTP->WrCnt += 1;
			break;
		case DO_QUIT:
		default:
			goto out;
			break;
		}
	}
	
out:

	pthread_exit(0);
	return 0;
}


#endif


int g_iTotalFree = 0;
char g_sDiskName[64];

WAVEFILEHDR		g_fileHdr;
WAVEFACTHDR		g_factHdr;
WAVEDATAHDR		g_dataHdr;

unsigned char *g_pReadData = NULL, *g_ptrRead;		// read for record buf
unsigned char *g_pEncodedData1 = NULL, *g_ptrEncode;	// data for write to file
unsigned char *g_pEncodedData2 = NULL;	// data for write to file
unsigned long g_lReadedLen = 0;			// all read from dsp
unsigned long g_lEncodedLen = 0;		// all incode for this file
unsigned char *shm_ptr = NULL;		// share memory addr

int g_bQuit = 0;
/*****************************************************/
			// recvlen, send len, recv p, send p
CommandInfo g_Commads[] = {
	{CMD_START, 4, 5, "rcOK", "start" },
	{CMD_PAUSE, 4, 5, "pause", NULL },
	{CMD_QUIT, 4, 12, "quit", "ANS_OVERFILE" },
	{CMD_TIME, 17, 10, "get_time_length", "ANS_LENGTH" },
	{CMD_NULL, 0, 0, NULL, NULL},
};

CmdCtrl g_CmdCtrl;

int GetDiskFree()
{
	struct statfs stbuf;

#if 1 //not check freespace
	g_iTotalFree = 10000;
#else
	if (statfs(g_sDiskName, &stbuf) == -1)
	{
		PERROR("Unable to get disk space'%s':%s\n", g_sDiskName, strerror(errno));
		return -1;
	}
	g_iTotalFree = (stbuf.f_bsize / 256) * (stbuf.f_bfree / 4) / 1024;
#endif
//	PMSG("g_iTotalFree = %d\n", g_iTotalFree);
	return g_iTotalFree;
}

void Messagehelp()
{
	printf("Usage:\n");
	printf("\trecord [ -omidrnfph ]\n");
	printf("\t\t-o: record file path\n");
	printf("\t\t-m: mixer device, default is: /dev/mixer\n");
	printf("\t\t-i: set record source[line|mic], default is:mic\n");
	printf("\t\t-d: autio device, default is: /dev/dsp\n");
	printf("\t\t-r: set record bitrate, default is:44100\n");
	printf("\t\t-n: set channel num[1|2], default is: 2(stero)\n");
	printf("\t\t-f: record file format[1|17], default is: 11(ima_adpcm)\n");
	printf("\t\t-p: record disk name, default is: /mnt/disk/MMCSD\n");
	printf("\t\t-h: printf help\n\n");
	fflush(stdout);
}


#define DEV_DSP		"/dev/dsp"

int main(int argc, char *argv[])
{
	char sFile[256], sDevMix[64], sDevDsp[64];
	int iSource = 0, iSetSource = 0;
	int iChannels = 2;	// 0 for mic, 1 for line
	int iFormat = WAVE_FORMAT_IMA_ADPCM;	// 11 or 1
	
	int fdDev = -1;
	FILE *fp = NULL;
	
	int iRet = -1, i, iTmp, err;
	int r_time = 0;
	int bPaused = 0;

	int iEncodeBuf;				// 0 for g_pDecodedData1 , 1 for g_pDecodedData2
	int iEncodedSize;
	int iSarcoEncode, iOneEncode, iOneEncoded;// onc time decode byte, all current decoded for write
	int iReadData, iReadStart, iReadSize;
	
	int iBitRates[] = {8000, 11025, 22050, 44100};	// can select bitrate
	int iBitRate = 3;//
	int iAvgBytePerSec;
	int iBlockAlign = 2048;
	int iSamplePerBlock = 0;

	int blocksize = 8192;

	int iState[16];
	
	PVERSION("Optimized version of record for BF533-PMP.");
	setpriority(PRIO_PROCESS, 0, -20);
	
	memset(sFile, 0, sizeof sFile);
	sprintf(sDevMix, "%s", "/dev/mixer");
	sprintf(sDevDsp, "%s", "/dev/dsp");
	sprintf(g_sDiskName, "/mnt/disk/MMCSD");
	
	err = 0;
	opterr = 0;	// prevent error put 	
	while((iTmp = getopt(argc, argv, "o:m:i:d:r:n:f:h:p:")) != -1)
	{
		switch(iTmp)
		{
		case 'o':		// output file
			sprintf(sFile, "%s", optarg);	
			break;
		case 'm':
			memset(sDevMix, 0, sizeof sDevMix);
			sprintf(sDevMix, "%s", optarg);
			break;
		case 'i':
			if(0 == strcasecmp(optarg, "mic"))
				iSource = 0;
			if( 0 == strcasecmp(optarg, "line"))
				iSource = 1;
			iSetSource = 1;
			break;
		case 'd':
			memset(sDevDsp, 0, sizeof sDevDsp);
			sprintf(sDevDsp, "%s", optarg);
			break;
		case 'r':
			if( optarg)
			{
				iBitRate = atoi(optarg);
				for(i = 0; i < 4; i++)
					if( iBitRate == iBitRates[i])
						iBitRate = i;
				if( iBitRate >= 4)
				{
					PERROR("unknow bitrate:%d\n", iBitRate);
					err = 1;
				}	
			}	
			break;	
		case 'n':
			if( optarg)
				iChannels = atoi(optarg);
			if( iChannels != 1 && iChannels != 2)	
			{
				PERROR("record channel must be 1 or 2]\n");
				err = 1;
			}	
			break;
		case 'f':
			if( optarg)
			{
				iFormat = atoi(optarg);
				if( iFormat != WAVE_FORMAT_IMA_ADPCM &&
					iFormat != WAVE_FORMAT_PCM)
				{
					PERROR("record file format error\n");
					err = 1;
				}
			}
			break;
		case 'h':
		case '?':
			break;
		case 'p':
			memset(g_sDiskName, 0, sizeof g_sDiskName);
	        sprintf(g_sDiskName,"%s",optarg);
			break;
		default:
			err = 1;
		}
	}
	if(err || strlen(sFile)< 1)
	{
		Messagehelp();
		return -1;
	}
	
	if(! rcInitCmdCtrl(&g_CmdCtrl))
		return -1;
		
	if( iSetSource)
	{
		if(iSource){
			system("echo 19 fc > /proc/driver/audregs");
			system("echo 20 00 > /proc/driver/audregs");
		}else{
			system("echo 19 fc > /proc/driver/audregs");
			system("echo 20 60 > /proc/driver/audregs");
		} //lzcx

	 	if((fdDev = open(sDevMix, O_WRONLY, 0777)) < 0)
		{
			PERROR("ERROR: failed to open mixer device %s\nno=%d:%s\n", sDevMix, 
				errno, strerror(errno));
			return -1;
		}
			// set mic or line here
		close(fdDev), fdDev = -1;
	}

	initImaTable();
	for(i = 0; i < 16; i++)
		iState[i] = 0;
	
	g_pReadData = (unsigned char *)malloc(127 * BLOCK_SIZE);		// 127K
	g_pEncodedData1 = (unsigned char*)malloc(511 * BLOCK_SIZE);	// 511K
	g_pEncodedData2 = (unsigned char*)malloc(511 * BLOCK_SIZE);	// 511K

	if( !g_pReadData || !g_pEncodedData1 || !g_pEncodedData2)
	{
		PERROR("error malloc for read or decoded buf\n");
		goto out;
	}
	
	iEncodeBuf = 0;
	g_ptrEncode = g_pEncodedData1;
	iEncodedSize = 0;

#ifndef SARCO
	iBitRate = 3;
#else
	InitBR8kTable();
#endif

	iBitRate = iBitRates[iBitRate];
	PMSG("FileName=%s, format=%d, iBitRate = %d, iChannel = %d\n", sFile,
		iFormat, iBitRate, iChannels);
		
	if ((fdDev = open(sDevDsp, O_RDONLY, 0777)) < 0)
	{
		PERROR("ERROR: failed to open DAC device %s\nno=%d:%s\n", sDevDsp, 
			errno, strerror(errno));
		goto out;
	}
	
//	ioctl(fdDev, SNDCTL_DSP_RESET, 0);
	if( ioctl(fdDev, SNDCTL_DSP_SYNC, NULL) == -1)
		PERROR("SNDCTL_DSP_SYNC\n");
//	ioctl(fdDev, SNDCTL_DSP_SETFMT, &iFormat);
	iRet = 2;
	if( ioctl(fdDev, SNDCTL_DSP_CHANNELS, &iRet) == -1)
		PERROR("SNDCTL_DSP_CHANNELS=%d\n", iRet);
	iRet = 16;
	if( ioctl(fdDev, SNDCTL_DSP_SETFMT, &iRet) == -1 ) 
		PERROR("SNDCTL_DSP_SETFMT= %d\n", iRet);
	iRet = 44100;
	//iRet = iBitRate; //lzcx
	if( ioctl(fdDev, SNDCTL_DSP_SPEED, &iRet) == -1)
		PERROR("SNDCTL_DSP_SPEED=%d\n", iRet);
	if( ioctl(fdDev, SNDCTL_DSP_GETBLKSIZE, &blocksize) == -1 )
		PERROR("SNDCTL_DSP_GETBLKSIZE =%d\n", blocksize);

// file hdr
	g_fileHdr.riffhdr.riffid = WAV_RIFFTYPE;	// "RIFF"
	g_fileHdr.riffhdr.len = 0;					// filelen - 8
	g_fileHdr.riffhdr.waveid = WAV_WAVETYPE;    // "WAVE"
	g_fileHdr.riffhdr.fmtid  = WAV_FMTTYPE;     // "fmt "
	g_fileHdr.format.wFormatTag = iFormat;
	g_fileHdr.format.nChannels = iChannels;
	g_fileHdr.format.nSamplesPerSec = iBitRate;

// fact hdr
	g_factHdr.factid = WAV_FACTTYPE;
	g_factHdr.size = 4;
	g_factHdr.SampleLength = 0;

// data hdr
	g_dataHdr.dataid = WAV_DATATYPE;
	g_dataHdr.size = 0;

	switch (iFormat)
	{
	case WAVE_FORMAT_PCM:
		iTmp = 38;
		g_fileHdr.riffhdr.hdrlen = 18;
		g_fileHdr.format.nAvgBytesPerSec = 176400;
		g_fileHdr.format.wBitsPerSample = 16;
		g_fileHdr.format.cbSize = 0x0;
		g_fileHdr.format.nBlockAlign = 4;
		g_fileHdr.format.nSamplesPerBlock = 0;		// no used
		break;
	case WAVE_FORMAT_IMA_ADPCM:
		iTmp = 40;		
		iBlockAlign = iBitRate / 11000;		// 11k
		if( iBlockAlign < 1)
			iBlockAlign = 1;
		iBlockAlign = iBlockAlign * iChannels * 256;	
		iSamplePerBlock = iBlockAlign;
		iSamplePerBlock -= (4 * iChannels);
		iSamplePerBlock /= (4 * iChannels);
		iSamplePerBlock = iSamplePerBlock * 8 + 1;
		iAvgBytePerSec = iBitRate * iBlockAlign / iSamplePerBlock;
		
		g_fileHdr.riffhdr.hdrlen = 20;
		g_fileHdr.format.wBitsPerSample = 4;//16;
		g_fileHdr.format.cbSize = 0x02;
		g_fileHdr.format.nBlockAlign = iBlockAlign;
		g_fileHdr.format.nSamplesPerBlock = iSamplePerBlock;
		g_fileHdr.format.nAvgBytesPerSec = iAvgBytePerSec;
		break;
	default:		// not run here
		break;
	}

#ifdef USE_SWAP
	g_fileHdr.riffhdr.riffid = SWAPLONG(g_fileHdr.riffhdr.riffid);
	g_fileHdr.riffhdr.waveid = SWAPLONG(g_fileHdr.riffhdr.waveid);
	g_fileHdr.riffhdr.fmtid = SWAPLONG(g_fileHdr.riffhdr.fmtid);
	g_factHdr.factid = SWAPLONG(g_factHdr.factid);
	g_dataHdr.dataid = SWAPLONG(g_dataHdr.dataid);
#endif	

#if 0
	g_fileHdr.format.wFormatTag = SWAPSHORT(g_fileHdr.format.wFormatTag);
    g_fileHdr.format.nChannels = SWAPSHORT(g_fileHdr.format.nChannels);
    g_fileHdr.format.nSamplesPerSec = SWAPLONG(g_fileHdr.format.nSamplesPerSec);
	g_fileHdr.riffhdr.len = SWAPLONG(g_fileHdr.riffhdr.len);
	g_factHdr.size = SWAPLONG(g_factHdr.size);
	g_dataHdr.size = SWAPLONG(g_dataHdr.size);
	g_fileHdr.riffhdr.hdrlen = SWAPLONG(g_fileHdr.riffhdr.hdrlen);
	g_fileHdr.format.nAvgBytesPerSec = SWAPLONG(g_fileHdr.format.nAvgBytesPerSec);
	g_fileHdr.format.wBitsPerSample= SWAPSHORT(g_fileHdr.format.wBitsPerSample);
	g_fileHdr.format.cbSize = SWAPSHORT(g_fileHdr.format.cbSize);
	g_fileHdr.format.nBlockAlign = SWAPSHORT(g_fileHdr.format.nBlockAlign);
	g_fileHdr.format.nSamplesPerBlock = SWAPSHORT(g_fileHdr.format.nSamplesPerBlock);
#endif	
	
	if( (i = open(sFile, O_WRONLY | O_CREAT, 0777)) < 0)
	{
		PERROR("failed to open file:%s, %d:%s\n", sFile,
			errno, strerror(errno));
		goto out;
	}
	if( fcntl(i, F_SETFL, O_NONBLOCK) < 0)
	{
		PERROR("failed to set noblock fd\n");
		goto out;
	}

	if((fp = fdopen(i, "w")) == NULL)
	{
		PERROR("fdopen error\n");
		goto out;
	}
	
	if (1 != fwrite(&g_fileHdr, iTmp, 1, fp))
	{
		PERROR("ERROR: write g_fileHdr, errno=%d\n", errno);
		goto out;
	}
	if (1 != fwrite(&g_factHdr, sizeof(g_factHdr), 1, fp))
	{
		PERROR("ERROR: write() facthdr, errno=%d\n", errno);
		goto out;
	}
	if (1 != fwrite(&g_dataHdr, sizeof(g_dataHdr), 1, fp))
	{
		PERROR("ERROR: failed to write() datahdr, errno=%d\n", errno);
		goto out;
	}

	PMSG("Write header end, iBlockAlign=%d, iSamplePerBlock=%d\n", 
		iBlockAlign, iSamplePerBlock);

	//g_iTotalFree = GetDiskFree();
#ifdef USE_PTHREAD
	memset(&gThreadParam, 0, sizeof(gThreadParam));
	gThreadParam.fp = fp; 
	gThreadParam.iDoing = DO_NONE;
	gThreadParam.usleep = 1000;
	gThreadParam.WrAddrs[0] = (unsigned long)g_pEncodedData1;
	gThreadParam.WrAddrs[1] = (unsigned long)g_pEncodedData2;

	if (0 == pthread_attr_init(&gThreadParam.pth_attr)
		&& (0 == pthread_attr_setdetachstate(&gThreadParam.pth_attr,
			PTHREAD_CREATE_JOINABLE)) ) {
		
		if(pthread_create(&gThreadParam.pth, &gThreadParam.pth_attr, 
			(void*(*)(void*))pth_write_file, &gThreadParam))	{
			PERROR("pthread_create error\n");
		}
		else
			gThreadParam.use = 1;
	} 
	else
		PERROR("pthread_attr_init failed\n");
#endif
	/* Output data */
	switch (iFormat)
	{
	case WAVE_FORMAT_PCM:
	{
		PMSG("Start record, WAVE_FORMAT_PCM, %d\n", iRet);
		for (i = 0; i < 50; i++)
		{
			if ((iReadSize = read(fdDev, g_ptrEncode, blocksize)) > 0)
			{
				g_lReadedLen += iReadSize;
				if (fwrite(g_ptrEncode, iReadSize, 1, fp) != 1)
				{
					PERROR("ERROR: write file failed, errno=%d\n", errno);
				}
				
				iEncodeBuf = ((iEncodeBuf == 1) ? 0 : 1);
				g_ptrEncode = ((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2);

				PMSG("writed:i=%d,size=%ld,len=%d\n",i, g_lReadedLen, iReadSize);
			}
			else
			{
				PERROR("read errror\n");
			}
		}
//		ioctl(fdDev, RXDMA_CLEAN);

		fseek(fp, 0, SEEK_SET);
		g_fileHdr.riffhdr.len = g_lReadedLen + 58 - 8;
		g_factHdr.size = g_lReadedLen;
		g_dataHdr.size = g_lReadedLen;


		fseek(fp, 4, SEEK_SET);
		fwrite(&g_fileHdr.riffhdr.len, sizeof(unsigned long), 1, fp);
		
		fseek(fp, 46, SEEK_SET);
		fwrite(&g_factHdr.size, sizeof(unsigned long), 1, fp);
		
		fseek(fp, 54, SEEK_SET);
		fwrite(&g_dataHdr.size, sizeof(unsigned long), 1, fp);
		
		PMSG("record end, filesize = %ld\n", g_lReadedLen+58);
		fclose(fp), fp = NULL;
	}
		break;

	case WAVE_FORMAT_IMA_ADPCM:
//		ioctl(fdDev, RXDMA_INIT);
		iReadStart = 0;
		iSarcoEncode = 2041 * 4;
		iOneEncode =  iSamplePerBlock * 4;
		iEncodedSize = 0;
		iReadData = iSarcoEncode * 4;		// it is div:2 4 5
		
		for(iReadStart = 0; iReadStart < 5; iReadStart ++)
			read(fdDev, g_pReadData, iReadData);
		iReadStart = 0;
		PMSG("channels = %d, bitrate = %d, iOneEncode = %d \n", iChannels, iBitRate, iOneEncode);

ReadLoop_11:

		if ((iReadSize = read(fdDev, g_pReadData+iReadStart, iReadData  - iReadStart)) > 0)
		{
//			PMSG("iEncodedSize = %ld,iReadSize= %ld,iOneEncode=%d,ReadStart=%d\n", 
//				g_lEncodedLen, g_lReadedLen, iOneEncode, iReadStart);
//			fflush(stdout);	
#ifdef SARCO
			iReadSize = SampleRateConvert(2, iReadSize, iBitRate, 
				g_pReadData+iReadStart, g_pReadData + iReadStart);
			if( iReadSize < 0)
			{
				PERROR("SampleRateConvert() .. retur:%d\n", iReadSize);
			}
#endif

			iReadSize += iReadStart;
			g_ptrRead = g_pReadData;
			
DecodeLoop_11:
			if(iReadSize < iOneEncode)
			{
				iReadStart = iReadSize;
				if( g_ptrRead != g_pReadData && iReadSize > 0)
					memcpy(g_pReadData, g_ptrRead, iReadSize);
				goto ReadLoop_11;
			}

			r_time = g_lReadedLen / (iBitRate * 4);
			iReadSize -= iOneEncode;
			g_lReadedLen += iOneEncode;

			if( 2 == iChannels)
				goto STERO;	

			for(i = 0; i < iOneEncode / 4; i++)		// drop other channel data
				((short *)g_ptrRead)[i] = (((short *)g_ptrRead)[2 * i]);
			
			ImaBlockMashI(1, (short *)g_ptrRead, iSamplePerBlock, iState, g_ptrEncode, 0);
			iOneEncoded = (iOneEncode - 4) / 4 + 2 * 4;
			iOneEncoded /= 2;
			goto CHANNEL_LAST;
			
STERO:
			ImaBlockMashI(2, (short *)g_ptrRead, iSamplePerBlock, iState, g_ptrEncode, 0);
			iOneEncoded = (iOneEncode - 4) / 4 + 2 * 4;
			goto CHANNEL_LAST;
			
CHANNEL_LAST:
			g_ptrRead += iOneEncode;

			g_lEncodedLen += iOneEncoded;
			iEncodedSize += iOneEncoded;
			g_ptrEncode += iOneEncoded;
		
ReadCmd:
	        g_iTotalFree = GetDiskFree();
			switch((iTmp = rcGetCommand()))
			{
			case CMD_START:
			case CMD_PAUSE:
				bPaused = ((1 == bPaused) ? 0 : 1);
//				rcSendCommand(&g_CmdCtrl, iTmp, -1);
				break;
			case CMD_QUIT:
				g_bQuit = 1;
				rcSendCommand(&g_CmdCtrl, iTmp, -1);
				break;
			case CMD_TIME:
				rcSendCommand(&g_CmdCtrl, CMD_TIME, r_time);
				break;
			case CMD_NULL:
			default:
				break;
			}

			if( 1 == bPaused && g_bQuit != 1)
			{
				usleep(10000);
				goto ReadCmd;
			}	

//			PMSG("incode All count=%ld, buf count=%d \n", g_lEncodedLen, iEncodedSize);

			if (g_bQuit || g_iTotalFree < 3)
			{
//				ioctl(fdDev, RXDMA_CLEAN);

				PMSG("bQuit=%d,diskfree=%d, filelen=%ld\n", 
					g_bQuit, g_iTotalFree, g_lEncodedLen+60);
				g_bQuit = 0;
				
#ifdef USE_PTHREAD
				if( gThreadParam.use)	{
					void *pth_ret;
					gThreadParam.WrLens[iEncodeBuf] = iEncodedSize;
					gThreadParam.iDoing = DO_WRITE1 + iEncodeBuf;
					gThreadParam.WrNo += 1;
					usleep(200000);
					gThreadParam.iDoing = DO_QUIT;
					iTmp = pthread_join(gThreadParam.pth, &pth_ret);
					PFUNC("Write Thread Return:%d\n", iTmp);
				}
				else {
					fwrite(((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2), 
						iEncodedSize, 1, fp);
					fsync(fileno(fp));
				}
#else
				fwrite(((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2), 
					iEncodedSize, 1, fp);
				
				sleep(1);		// wait last write over
#endif
				fseek(fp, 0, SEEK_SET);
				g_fileHdr.riffhdr.len = g_lEncodedLen + 60 - 8;
				g_factHdr.size = g_lReadedLen;
				g_dataHdr.size = g_lEncodedLen;
#if 0
				g_fileHdr.riffhdr.len = SWAPLONG(g_fileHdr.riffhdr.len);
				g_factHdr.size = SWAPLONG(g_factHdr.size);
				g_dataHdr.size = SWAPLONG(g_dataHdr.size);
#endif
				PMSG("---len=%ld,fact.size=%ld,data.size=%ld\n",
					g_fileHdr.riffhdr.len, g_factHdr.size, g_dataHdr.size);
				
				fseek(fp, 4, SEEK_SET);
				fwrite(&g_fileHdr.riffhdr.len, sizeof(unsigned long), 1, fp);

				fseek(fp, 48, SEEK_SET);
				fwrite(&g_factHdr.size, sizeof(unsigned long), 1, fp);

				fseek(fp, 56, SEEK_SET);
				fwrite(&g_dataHdr.size, sizeof(unsigned long), 1, fp);

				fclose(fp), fp = NULL;
			}
			else
			{
				if( iEncodedSize > (500*1024))
				{
#ifdef USE_PTHREAD
					if( gThreadParam.use)	{
						gThreadParam.WrLens[iEncodeBuf] = iEncodedSize;
						gThreadParam.iDoing = DO_WRITE1 + iEncodeBuf;
						gThreadParam.WrNo += 1;
					}
					else {
						fwrite(((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2), 
							iEncodedSize, 1, fp);
						fsync(fileno(fp));
					}
					iEncodedSize = 0;
#else
					fwrite(((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2), 
						iEncodedSize, 1, fp);
					iEncodedSize = 0;
					sync();//@ADD:chenee force flush buffer
					
#endif
					iEncodeBuf = ((iEncodeBuf == 1) ? 0 : 1);
					g_ptrEncode = ((iEncodeBuf == 0) ? g_pEncodedData1 : g_pEncodedData2);
				}
				
				goto DecodeLoop_11;
			}
		}
		else
		{
			PERROR("Read error, iReadSize = %d\n", iReadSize);
			g_bQuit = 1;
			goto ReadLoop_11;
		}
	
//		ioctl(fdDev, RXDMA_CLEAN);
		break;
	default:
		break;
	}
	
out:
	if( fdDev > 0)
		close(fdDev), fdDev = -1;

	if( g_pEncodedData1)
		free(g_pEncodedData1), g_pEncodedData1 = NULL;
	if( g_pEncodedData2)
		free(g_pEncodedData2), g_pEncodedData2 = NULL;
	if( g_pReadData)
		free(g_pReadData), g_pReadData = NULL;

#ifdef SARCO
	FreeBR8kTable();
#endif

	PMSG("record Exit !\n");
	sleep(1);
	if(g_iTotalFree < 3)
	   iRet=99; //to indicate that the recorder quit because the sd card is full
	return iRet;	
}


// recrod ctrl init cmd ctrl
int rcInitCmdCtrl(CmdCtrl *pCC)
{
	PFUNC("\n");
	ASSERT(pCC);

	pCC->fd[FD_RECV] = fileno(stdin);
	pCC->fd[FD_SEND] = fileno(stdout);
	pCC->iLen = 0;
	memset(pCC->sCmd, 0, SIZE_CMD);		
	if( -1 == pCC->fd[FD_RECV] || -1 == pCC->fd[FD_SEND])
	{
		PERROR("get FILE to fd error\n");
		return 0;
	}	
	else
		return 1;
}

// record ctrl close
void rcCloseCmdCtrl(CmdCtrl *pCC)
{
	int i;
	PFUNC("\n");
	ASSERT(pCC);
	for(i = 0; i < 2; i++)
	{
		if(-1 != pCC->fd[i])
			close(pCC->fd[i]), pCC->fd[i] = -1;
	}
}

// record send command
// in : iVal: -1 no param
// ret: 0 Error, else ok
int rcSendCommand(CmdCtrl *pCC, int iCmd, int iVal)
{
	int iRet;
	ASSERT(pCC);
	ASSERT((iCmd>=0) &&(iCmd<CMD_NULL));
	
	if(-1 == iVal)
		sprintf((char*)g_CmdCtrl.sCmd, "%s\n", g_Commads[iCmd].pSend);
	else
		sprintf((char*)g_CmdCtrl.sCmd, "%s%4d\n", g_Commads[iCmd].pSend, iVal);
	pCC->iLen = strlen((char*)g_CmdCtrl.sCmd);
	
	if( pCC->iLen == write(pCC->fd[FD_SEND], pCC->sCmd, pCC->iLen))
		iRet = 1;
	else
		iRet = 0;
	fflush(stdout);	
	PFUNC("send cmd:%s", g_CmdCtrl.sCmd);
	pCC->iLen = 0;
	memset(pCC->sCmd, 0, SIZE_CMD);
	return iRet;	
}

// get ctrl command
int rcGetCommand(void)
{
	int fd = g_CmdCtrl.fd[FD_RECV];
	fd_set rset;
	struct timeval tWait;
//	PFUNC("fd=%d\n", fd);
	
	FD_ZERO(&rset);
	FD_SET(fd, &rset);
	tWait.tv_sec = 0;
	tWait.tv_usec = 0;
		
	if(select(fd+1, &rset, NULL, NULL, &tWait))
		if(FD_ISSET(fd, &rset))
			goto ReadCmd;
	
	return CMD_NULL;

ReadCmd:
	{
		char sBuf[SIZE_CMD];
		int i = 0;
		fgets(sBuf, SIZE_CMD, stdin);
		if( strlen(sBuf) < 2)
			return CMD_NULL;
		
		for(i = 0; i <= CMD_NULL;  i++)
		{
			if(strncasecmp(sBuf, g_Commads[i].pRecv, g_Commads[i].iRecv))
				continue;
			PMSG("get command:i=%d:%s\n", i, g_Commads[i].pRecv);
			break;					
		}
		
		return i;
	}	
}


