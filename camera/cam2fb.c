/*
 * CAMIF test
 *
 * bushi@mizi.com
 *
 * $Id: cam2fb.c,v 1.1.1.1 2004/01/20 10:29:10 laputa Exp $
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <asm/types.h> 
#include <linux/videodev2.h>
#include <linux/fb.h>
/*
 * must be 
 *   CAM_WIDTH  == FB_WIDTH
 *   CAM_HEIGHT <= FB_HEIGHT
 */
#define QUIT_CMD	"quit"
#define TYPE_CMD	"type"
#define CAPT_CMD	"capt"
static unsigned int capframe = 0;
static unsigned char filename[30];
FILE *bmpFile;

unsigned char bmp_head_t[] = {
        0x42,0x4d,0x42,0x58,0x02,0x00,0x00,0x00,0x00,0x00,
        0x42,0x00,0x00,0x00,0x28,0x00,0x00,0x00,0xf0,0x00,
        0x00,0x00,0x40,0x01,0x00,0x00,0x01,0x00,0x10,0x00,
        0x03,0x00,0x00,0x00,0x00,0x58,0x02,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0xf8,0x00,0x00,0xe0,0x07,
        0x00,0x00,0x1f,0x00,0x00,0x00
};

//char rgb[640*480*4]; // MAX
static unsigned char *vf_buff;
static unsigned char *vf_buff1;
static int fb_bpp;
unsigned char bmp_head[54];

void writeImageToFile(unsigned int size)
{
	capframe++;
	sprintf(filename,"/tmp/0%d.bmp",capframe);
   	bmpFile=fopen(filename, "w+");
	if(fb_bpp == 16)
		fwrite(bmp_head_t,1,66,bmpFile);
	else
		fwrite(bmp_head,1,54,bmpFile);
   	fwrite(vf_buff,1,size,bmpFile);
   	fclose(bmpFile);
}

static int cam_fp = -1;
static int fb_fp = -1;
static char *fb_addr = NULL;
static char cam_name[50];
int width=0;
int height=0;

static inline int cam_init(void)
{
	int dev_fp = -1;

	dev_fp = open(cam_name, O_RDWR);
	if (dev_fp < 0) {
		printf("error open %s\n",cam_name);
		return -1;
	}

	return dev_fp;
}

static inline int fb_init(void)
{
	int dev_fp = -1;
	int fb_size;
	struct fb_var_screeninfo vinfo;

	dev_fp = open("/dev/fb0", O_RDWR);
	if (dev_fp < 0) {
		perror("/dev/fb0");
		return -1;
	}
	if (ioctl(dev_fp, FBIOGET_VSCREENINFO, &vinfo)) {
	        printf("Error reading variable information.\n");
		exit(1);
	}
	width=vinfo.xres;
	height=vinfo.yres;
	fb_bpp=vinfo.bits_per_pixel;
	if(fb_bpp==24) fb_bpp=32;
	fb_size=width*height*fb_bpp/8;
	if ((fb_addr = (char*)mmap(0, fb_size, 
			PROT_READ | PROT_WRITE, MAP_SHARED, dev_fp, 0)) < 0) {
		perror("mmap()");
		return -1;
	}
	printf("%dx%d bpp:%d mmaped 0x%08x\n",width,height,fb_bpp,fb_addr);

	return dev_fp;
}

static inline int read_data(int fp, char *buf, int width, int height, int bpp)
{
	int ret=0;

	if ((ret = read(fp, buf, (width*height*bpp/8))) != (width*height*bpp/8)) {
	    printf("read %d--0x%x\n",ret,ret);
		return 0;
	}

	return ret;
}

static inline void print_fps(struct timeval *s, struct timeval *e)
{
	unsigned long time;
	unsigned long sec;
	unsigned long usec;
	int fps = 0;
	
	sec = e->tv_sec - s->tv_sec;
	if (e->tv_usec > s->tv_usec)
		usec = e->tv_usec - s->tv_usec;
	else {
		usec = e->tv_usec + 1000000 - s->tv_usec;	
		sec--;
	}
	time = sec * 1000 + (usec+1) / 1000;
	
	if(time==0)
	    return;
	
	fps = 30000 / time;
//	printf("%d fps\n", fps);
}

int main(int argc, char *argv[])
{
	unsigned int frames = 0;
	struct timeval tv1;
	struct timeval start_tv, end_tv;
	struct timezone tz;
	int src_img;
	unsigned long size;
	int index=0;
	struct v4l2_capability cap;
	struct v4l2_input i;
	struct v4l2_framebuffer fb;
	int on=1;
	int tmp,m,n;
	fd_set fds1;
	int fd;
	char cmd[256];
	struct fb_var_screeninfo vinfo;
	
	if(argc!=2)
	{
	    printf("Usage:cam2fb [device node]\n");
	    return 0;
	}
	sprintf(cam_name,"%s",argv[1]);

	cam_fp = cam_init();
	fb_fp = fb_init();
	
	size=width*height*fb_bpp/8;

	if((tmp=ioctl(cam_fp, VIDIOC_QUERYCAP, &cap))<0) {
		printf("VIDIOC_QUERYCAP error, ret=0x%x\n",tmp);
		goto err;
	}
	printf("Driver:%s, Card:%s, cap=0x%x\n",cap.driver,cap.card,cap.capabilities);
	
	memset(&i, 0, sizeof(i));
	i.index=index;
	if(ioctl(cam_fp, VIDIOC_ENUMINPUT, &i)<0)
		goto err;
	printf("input name:%s\n",i.name);
	
	index=0;
	if(ioctl(cam_fp, VIDIOC_S_INPUT, &index)<0)
		goto err;

	if(ioctl(cam_fp, VIDIOC_S_OUTPUT, &index)<0)
	        goto err;

	if(ioctl(cam_fp, VIDIOC_G_FBUF, &fb)<0)
		goto err;
	printf("g_fbuf:capabilities=0x%x,flags=0x%x,width=%d,height=%d\npixelformat=0x%x,bytesperline=%d,colorspace=%d,base=0x%x\n",
		fb.capability,fb.flags,fb.fmt.width,fb.fmt.height,fb.fmt.pixelformat,
		fb.fmt.bytesperline,fb.fmt.colorspace,fb.base);
	fb.capability = cap.capabilities;
	fb.fmt.width =width;
	fb.fmt.height = height;
	fb.fmt.pixelformat = (fb_bpp==32)?V4L2_PIX_FMT_BGR32:V4L2_PIX_FMT_RGB565;

	if(ioctl(cam_fp, VIDIOC_S_FBUF, &fb)<0)
		goto err;
	on = 1;
	if(ioctl(cam_fp, VIDIOC_OVERLAY, &on)<0)
		goto err;
	
	vf_buff = (char*)malloc(size);
	if(vf_buff==NULL)
		goto err;

	unsigned short temp;

	if(fb_bpp==16){
	    *((unsigned int*)(bmp_head_t+18)) = width;
	    *((unsigned int*)(bmp_head_t+22)) = height;
	    *((unsigned short*)(bmp_head+28)) = 16;
	}else{
	bmp_head[0] = 'B';
	bmp_head[1] = 'M';
	*((unsigned int*)(bmp_head+2)) = (width*fb_bpp/8)*height+54;
	*((unsigned int*)(bmp_head+10)) = 54;
	*((unsigned int*)(bmp_head+14)) = 40;
	*((unsigned int*)(bmp_head+18)) = width;
	*((unsigned int*)(bmp_head+22)) = height;
	*((unsigned short*)(bmp_head+26)) = 1;
	*((unsigned short*)(bmp_head+28)) = fb_bpp;
	*((unsigned short*)(bmp_head+34)) = (width*fb_bpp/8)*height;
	}
	gettimeofday(&start_tv, &tz);
	while(1)
	{
	    if (!read_data(cam_fp, &vf_buff[0], width, height, fb_bpp))
	    {
		printf("read error\n");
	    }
	    int i=0;
	    memcpy(&fb_addr[0],vf_buff,width*height*fb_bpp/8);
	    frames++;
	    if((frames%30)==0){
		gettimeofday(&end_tv, &tz);
		print_fps(&start_tv, &end_tv);
		gettimeofday(&start_tv, &tz);
	    }
	    fd=0;
	    tv1.tv_sec=0;
	    tv1.tv_usec=0;
	    FD_ZERO(&fds1);
	    FD_SET(fd,&fds1);
	    select(fd+1,&fds1,NULL,NULL,&tv1);
	    if(FD_ISSET(fd,&fds1))
	    {
		memset(cmd,0,sizeof(cmd));
		read(fd,cmd,256);
		if(strncmp(cmd,"quit",4)==0){
		    printf("-->quit\n");
		    on=0;
		    if(ioctl(cam_fp, VIDIOC_OVERLAY, &on)<0)
			goto err;
		    close(fb_fp);
		    close(cam_fp);
		    return 0;
		}else if(strncmp(cmd,"capt",4)==0){
		    printf("-->capture\n");
		    printf("write to img --> ");
		    writeImageToFile(size);
		    printf("OK\n");
		}
	    }
	}

err:
	if (cam_fp)
		close(cam_fp);

end:

	return 0;
}
