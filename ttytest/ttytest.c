/***************************************************************************
                          main.c  -  description
                             -------------------
    begin                : Wed Jun 19 16:03:39 CST 2002
    copyright            : (C) 2002 by wf
    email                : root@localhost.localdomain
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#ifndef _RS232_H
//#define _RS232_H
#include "rs232.h"
//#endif

main(int argc, char *argv[])
{

	int ret,portno,nWritten,nRead;
	char com[10],err[50];
	char buf[256];
	portno=0;

	if(argc>1)
		sprintf(com,"/dev/ttyS%s",argv[1]);
	else
		sprintf(com,"/dev/ttyS1");
//	while(1)
//	{
		//ret=OpenCom(portno,com,115200);
		ret=OpenCom(portno,com,115200);
		if(ret==-1)
		{
			sprintf(err,"The %s open error.",com);
			perror(err);
			exit(1);
		}
#if 1
		nWritten=ComWrt(portno,"abcdefghijklmnopqrstuvwxyz",26);
		printf("/dev/ttyS1 has send %d chars!\n",nWritten);
		printf("\nRecieving data!********\n");
		fflush(stdout);
#endif
		while(1) {
		nRead=ComRd(0,buf,256,3000);
//		if(nRead>0)
//		{
//			printf("*****OK\n");
//		}
//		else
//			printf("Timeout\n");
		}
		if((ret=CloseCom(portno)==-1))
		{
			perror("Close com");
			exit(1);
		}
		printf("\n\n");
//	}
	printf("Exit now.\n");
	return;
}
