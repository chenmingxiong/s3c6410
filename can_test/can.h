#ifndef   _CAN_H
#define  _CAN_H

#define RXBUFFERSIZE 100
#define	 SITE_MAX	256

//节点机CAN ID
#define LOCAL_ID					 0x10

//命令:广播节点机地址
#define BROADCAST_NODE_ID            0x8B
#define BROADCAST_NODE_ID_ACK        0xA2

//命令:校时
#define TIME_SYNC				 	 0xF5

#define SET_DATE                	 0x83 //TIME1
#define SET_TIME                	 0x84 //TIME2

#define TIME_SYNC_ACK1			 	 0xF3
#define TIME_SYNC_ACK2			 	 0xF4

//命令:心跳
#define BREATHE_ACK					 0xC0

//命令:签到
#define ISSU						 0xC2
#define ISSU_ACK					 0xC3

//命令:交易记录重发
#define QUER_REC					 0xC4
#define SITE_RECORD_UPLOAD_ACK	 	 0x89
#define SITE_RECORD_UPLOAD_AGAIN	 0xC4

//命令: 	查询桩机自行车编号
#define QUER_BIKE					 0x99
#define QUER_BIKE_ACK				 0xB0

//命令:桩机控制:开锁/上锁
#define SITE_LOCK_CONTROL			 0x81
#define SITE_LOCK			 		 0xFF
#define SITE_UNLOCK			 		 0x00
#define SITE_LOCK_CONTROL_ACK		 0x82

//命令:桩机控制:挂起/恢复
#define SITE_HANG_UP_CONTROL	 	 0x8F
#define SITE_HANG_UP_CONTROL_ACK	 0x90

//命令:参数设置
#define SITE_PARAMETER_SET			 0x8A
#define SITE_PARAMETER_SET_ACK		 0x8B

//命令:FLASH初始化
#define SITE_INITIAL_FLASH			 0x98
#define SITE_INITIAL_FLASH_ACK		 0xA3

//命令:黑名单下载
#define SITE_BLACK_LIST_DOWNLOAD	 0x8D
#define SITE_BLACK_LIST_DOWNLOAD_ACK 0xA0
#define SITE_BLACK_LIST_TYPE_MAIN	 0x00
#define SITE_BLACK_LIST_TYPE_ADD	 0x01
#define SITE_BLACK_LIST_TYPE_ERASE	 0x02

//命令:捕获黑名单
#define SITE_BLACK_LIST_CATCH1	 	 0x60
#define SITE_BLACK_LIST_CATCH2	 	 0x61
#define SITE_BLACK_LIST_CATCH3	 	 0x62
#define SITE_BLACK_LIST_CATCH_ACK 	 0x89

//命令:固件升级
#define SITE_FIRMWARE_UPDATE		 0x8E
#define SITE_FIRMWARE_UPDATE_ACK	 0xA1

//命令:交易记录上传
#define SEND_REC1                    0x00
#define SEND_REC2                    0x01
#define SEND_REC3                    0x02
#define SEND_REC4                    0x03
#define SEND_REC5                    0x04
#define SEND_REC6                    0x05
#define SEND_REC7                    0x06
#define SEND_REC8                    0x07
#define SEND_REC9                    0x08
#define SEND_REC10                   0x09
#define SEND_REC11                   0x0A
#define SEND_REC12                   0x0B
#define SEND_REC13                   0x0C
#define SEND_REC14                   0x0D
#define SEND_REC_ACK				 0x89

#define CAN_MSG_LENGTH 8		/**< maximum length of a CAN frame */
/**
* The CAN message structure.
* Used for all data transfers between the application and the driver
* using read() or write().
*/
typedef struct {
    /** flags, indicating or controlling special message properties */
    int             flags;
    int             cob;	 /**< CAN object number, used in Full CAN  */
    unsigned   long id;		 /**< CAN message ID, 4 bytes  */
    struct timeval  timestamp;	 /**< time stamp for received messages */
    short      int  length;	 /**< number of bytes in the CAN message */
    unsigned   char data[CAN_MSG_LENGTH]; /**< data, 0...8 bytes */
} canmsg_t;

void *can_listen(void * data);
void site_lock_control(unsigned char site_id, unsigned char lock_control_type);
void write_can(unsigned char site_id, unsigned char command_num,unsigned char *data);
#endif
