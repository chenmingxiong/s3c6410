#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>
#include <malloc.h>
#include <time.h>
#include <linux/can/can.h>

#define MAX_CAN_LIST_LENGTH 1024
#define CAN_DEVICE "/dev/mcp2515"

canmsg_t rx[RXBUFFERSIZE];
int can_fd;
int messages_to_read = RXBUFFERSIZE;
unsigned char time_sync_flags[SITE_MAX]={0};

//can初始化
int can_init(void)
{
	debug("%s:%d\n",__FUNCTION__,__LINE__);

	can_fd = open(CAN_DEVICE, O_RDWR | O_NONBLOCK); 
	if(can_fd<0)
	{
		error("Open %s fail!\n",CAN_DEVICE);
		perror("open");
		return(-1);
	}
	return 0;
}

// 写CAN函数
void write_can(unsigned char site_id, unsigned char command_num,unsigned char *data)
{
	int ret;
	unsigned char i;
	canmsg_t tx;

	debug("%s:%d\n",__FUNCTION__,__LINE__);

	if((site_id!=0xFF)&&(site_id_check(site_id) == -1))
		return;
	
	debug("now write CAN: ");
	
	pthread_mutex_lock(&write_can_lock);
	
	tx.id = site_id;
	tx.flags=0;
	tx.cob=0;
	tx.length = 8;
	for(i=0; i<command_num; i++)
	{
		memcpy(tx.data,data+i*8,8);
		ret = write(can_fd, &tx, 1);
		if(ret == -1)
		{
			error("CAN write error.###");
		} 
		usleep(5000);
	}
}

//CAN接收数据/处理线程
void *can_read(void * data)
{
    unsigned char site_record_buff[SITE_MAX][78];
	unsigned char black_list_catch_data[SITE_MAX][20];
	int site_record_flag[SITE_MAX];
	int command_type;
	int len_msg;
	int site_num; 
	int can_main_fifo_fd;
	unsigned char i,got;
	unsigned int n=0;
		
	debug("%s:%d\n",__FUNCTION__,__LINE__);

	memset(site_record_flag, 0, SITE_MAX);

	can_main_fifo_fd=open(CAN_MAIN_FIFO,O_RDWR);
	if(can_main_fifo_fd<0)
	{
		error("open %s fail!\n",CAN_MAIN_FIFO);
	}

	while(1)
	{
		got = read(can_fd, &rx, messages_to_read);
		n+=got;
		debug("total receive %d \n", n);
		for(i = 0; i < got; i++) 
		{
			debug("recevie   %d \n", got);
			debug("id   %d \n", rx[i].id);
			debug("rx[%d].data[0]: %d\n",i,rx[i].data[0]);
			debug("rx[%d].data[1]: %d\n",i,rx[i].data[1]);
			debug("rx[%d].data[2]: %d\n",i,rx[i].data[2]);
			debug("rx[%d].data[3]: %d\n",i,rx[i].data[3]);
			debug("rx[%d].data[4]: %d\n",i,rx[i].data[4]);
			debug("rx[%d].data[5]: %d\n",i,rx[i].data[5]);
			debug("rx[%d].data[6]: %d\n",i,rx[i].data[6]);
			debug("rx[%d].data[7]: %d\n",i,rx[i].data[7]);
		}
		usleep(1000);
	}
}
